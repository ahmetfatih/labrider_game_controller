#!/usr/bin/env python

import rospy
from std_msgs.msg import String
import json
from gazebo_msgs.srv import GetModelState
from std_srvs.srv import Trigger, TriggerResponse
import time

class GameController:

    def __init__(self, robot_name):

        self.robot_name = robot_name

        self.robot_status = "Paused"

        self.point_a = ['-4.5', '-3.5']
        self.point_b = ['-3.5', '-0.5']
        self.point_c = ['1.5', '3.5']
        self.point_d = ['4.5', '-0.5']

        self.departure_point = "A"
        self.destination_point = "B"

        self.metrics = {
            'Robot Name' : self.robot_name,
            'Robot Status' : self.robot_status,
            'Departure Point' : self.departure_point,
            'Destination Point' : self.destination_point
        }

        rospy.init_node("game_controller")

        self.metric_pub = rospy.Publisher('simulation_metrics', String, queue_size=10)
        metric_msg_rate = 10
        rate = rospy.Rate(metric_msg_rate)

        rospy.Subscriber("robot_pose", String, self.pose_callback)

        s = rospy.Service('/robot_handler', Trigger, self.handle_robot)

        try:
            rospy.wait_for_service("/gazebo/get_model_state", 1.0)
            robot_check = rospy.ServiceProxy("/gazebo/get_model_state", GetModelState)

            resp = robot_check("labrider", "")
            if not resp.success == True:
                self.robot_status = "no_robot"

        except (rospy.ServiceException, rospy.ROSException), e:
            self.robot_status = "no_robot"
            rospy.logerr("Service call failed: %s" % (e,))


        while ~rospy.is_shutdown():
            self.publish_metrics()
            rate.sleep()
            if self.robot_status == "Stop":
                break


    def handle_robot(self,req):
        if self.robot_status == "Paused":
            self.robot_status = "Running"
            return TriggerResponse(
                success = True,
                message = self.robot_status
            )

        elif self.robot_status == "Running":
            return TriggerResponse(
                success = True,
                message = self.robot_status
            )

        elif self.robot_status == "Stop":
            return TriggerResponse(
                success = True,
                message = self.robot_status
            )

        elif self.robot_status == "no_robot":
            return TriggerResponse(
                success = True,
                message = self.robot_status
            )

    def publish_metrics(self):
        self.metrics['Robot Status'] = str(self.robot_status)
        self.metrics['Destination Point'] = self.destination_point
        self.metrics['Departure Point'] = self.departure_point
        self.metric_pub.publish(json.dumps(self.metrics, sort_keys=True))

    def pose_callback(self, data):
        pose_data = data.data.split(":")
        if pose_data[1] == self.point_a[0] and pose_data[2] == self.point_a[1]:
            self.departure_point = "A"
            self.destination_point = "B"
        elif pose_data[1] == self.point_b[0] and pose_data[2] == self.point_b[1]:
            self.departure_point = "B"
            self.destination_point = "C"
        elif pose_data[1] == self.point_c[0] and pose_data[2] == self.point_c[1]:
            self.departure_point = "C"
            self.destination_point = "D"
        elif pose_data[1] == self.point_d[0] and pose_data[2] == self.point_d[1]:
            self.robot_status = "Stop"

if __name__ == '__main__':
    # Name of robot
    controller = GameController("labrider")
